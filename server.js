exports.setup = function() {
    var app = require('express')();
    var http = require('http').Server(app);
    var port = process.env.PORT || 4000;
    var env = process.env.NODE_ENV || 'development';

    // http server
    function createHttpServer() {
        app.use(require('connect-livereload')());
        http.listen(port);
        app.use(require('cors')()); // enable CORS
        console.log('Server started on port %s', port);
    }

    // json body
    function configureJsonBody() {
        var bodyParser = require('body-parser');
        app.use(bodyParser.urlencoded({ extended: true }));
        app.use(bodyParser.json());
    }

    createHttpServer();
    configureJsonBody();

    return app;
};
