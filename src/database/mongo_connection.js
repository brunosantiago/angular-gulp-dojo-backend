var single_connection_mongo = false;

module.exports = function () {
  var mongoose = require('mongoose');
  var mongoUrl = 'mongodb://treinamento:treinamento@ds059644.mongolab.com:59644/dojo';

  if (!single_connection_mongo) {
    single_connection_mongo = true;
    mongoose.connect(mongoUrl, function (err) {
      if (err) {
        single_connection_mongo = false;
        console.error('MongoDB connection error: ' + err);
        process.exit();
      } else {
        console.log('MongoDB connected');
      }
    });
  }
  return mongoose;
};
