var Task = require('./model');
var HttpStatus = require('http-status-codes');

exports.findById = function (req, res, next) {
  var id = req.params.id;
  if (!id) {
    handleBadRequest(res);
    return;
  }
  Task.findOne({'_id': id}, function (err, data) {
    if (err) {
      handleError(res);
      return;
    }
    if (!data) {
      handlerNoContent(res);
      return;
    }
    res.json(data);
  });
};

exports.findAll = function (req, res, next) {
  Task.find({}, function (err, data) {
    if (err) {
      handleError(res);
      return;
    }
    if (!data || data.length <= 0) {
      handlerNoContent(res);
      return;
    }
    res.json(data);
  });
};

exports.update = function (req, res, next) {
  if (!req.body) {
    handleBadRequest(res);
    return;
  }

  var id = req.body._id;
  var name = req.body.name;

  if (!id || !name) {
    handleBadRequest(res);
    return;
  }

  Task.findByIdAndUpdate(id, { $set: { name: name }}, function (err, task) {
    if (err) {
      handleError(res);
      return;
    }
    handlerNoContent(res);
  });
};

exports.save = function (req, res, next) {
  if (!req.body || !req.body.name) {
    handleBadRequest(res);
    return;
  }

  var task = new Task(req.body);
  task.save(function (err) {
    if (err) {
      handleError(res);
      return;
    }
    handlerNoContent(res);
  });
};

exports.delete = function (req, res, next) {
  var id = req.params.id;

  if (!id) {
    handleBadRequest(res);
    return;
  }

  Task.remove({'_id': id}, function (err) {
    if (err) {
      next(err);
      return;
    }
    handlerNoContent(res);
  });
};

function handlerNoContent(res) {
  res.status(HttpStatus.NO_CONTENT).send();
}

function handleBadRequest(res) {
  res.status(HttpStatus.BAD_REQUEST).send();
}

function handleError(res) {
  res.status(HttpStatus.INTERNAL_SERVER_ERROR).send();
}
