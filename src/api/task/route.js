module.exports = function(apiRoutes) {
    var endpoint = '/task/';
    var controller = require('./controller');

    apiRoutes.get(endpoint+":id", controller.findById);
    apiRoutes.get(endpoint, controller.findAll);
    apiRoutes.post(endpoint, controller.save);
    apiRoutes.put(endpoint, controller.update);
    apiRoutes.delete(endpoint+":id", controller.delete);
};
