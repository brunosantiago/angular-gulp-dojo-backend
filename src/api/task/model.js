var mongoose = require('../../database/mongo_connection')();

var _id = function () {
    return new mongoose.Types.ObjectId();
};

var TaskSchema = new mongoose.Schema(
    {
        _id: {type: mongoose.Schema.ObjectId, default: _id},
        name: {type: String, required: true}
    }
);

module.exports = mongoose.model('Task', TaskSchema);
