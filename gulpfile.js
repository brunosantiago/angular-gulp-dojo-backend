var SOURCES_JS = [
    '!./node_modules/**/*.js',
    './src/**/*.js',
    './app.js'
];

var SOURCES_TEST = [
    './test/**/*.js'
];

var APP = './app.js';

// server
var gulp = require('gulp');

var server = require('gulp-express');
gulp.task('server', ['jshint'], function () {
    server.run([APP]);
    // watch
    gulp.watch(SOURCES_JS, function(event) {
        gulp.start('server');
        server.notify(event);
    });

});

// jshint
var jshint = require('gulp-jshint');
gulp.task('jshint', function () {
    var jshintSource = SOURCES_JS.concat(SOURCES_TEST);
    return gulp.src(jshintSource)
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});
